import React from 'react'
import logo from './icon/delivery.svg'
import { CartHeader } from './CartHeader'

export const MainMenu = () => {
    return (
        <div className="main-menu">
            <div className="container d-flex">
                <div className="logo">
                    <img src={logo} className="img-fluid" alt="Logo" />
                </div>
                <CartHeader />
            </div>
        </div>
    )
}
