import React, { useContext, useState} from 'react'
import imgPro from './images/pro.jpg'

import { CartContext } from '../Contexts/CartContext'

export const ProductHoz = () => {
    const context = useContext(CartContext)
    const [numb, setNumb] = useState(context.number)
    return (
        <div className="col col-3">
            <div className="productHoz">
                <div className="thumb">
                    <img src={imgPro} alt="Product" />
                </div>
                <div className="inf">
                    <h5 className="namePro">
                        <a href="#">Áo thun</a>
                    </h5>
                    <p className="price">500.000đ</p>
                    
                </div>
                <div className="action">
                    <CartContext.Consumer>
                        {({addToCart}) => <button onClick={ () => addToCart()} type="button" name="" id="" className="btn btn-primary">Add to cart</button>}
                    </CartContext.Consumer>
                    
                </div>
            </div>
        </div>
    )
}
