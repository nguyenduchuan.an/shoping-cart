import React from 'react'
import { ProductHoz } from './ProductHoz'

export const ListProduct = () => {
    return (
        <div className="container">
            <h2>Danh sách sản phẩm</h2>
            <div className="row">
                <ProductHoz />
            </div>
        </div>
    )
}