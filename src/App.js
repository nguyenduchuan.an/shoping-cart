import React from 'react';
import './App.css';
import { Home } from './Pages/Home';
import { CartProvider } from './Contexts/CartContext';


function App() {
  return (
    <CartProvider>
      <div className="App">
       
        <Home />
      </div>
    </CartProvider>
  );
}

export default App;
