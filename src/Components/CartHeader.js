import React, {useContext} from 'react'
import iconCart from './icon/shopping-cart.svg'
import { CartBox } from './CartBox'
import { CartContext } from '../Contexts/CartContext'
export const CartHeader = () => {
    const context = useContext(CartContext);
    return (
        <div className="cardHead">
            <img className="iconCart" src={iconCart} alt="Cart" />
            <CartContext.Consumer>
                {context => <div className="cartCount"> {context.number} </div>}
            </CartContext.Consumer>
            <CartBox />
        </div>
    )
}