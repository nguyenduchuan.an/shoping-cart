import React, { Component } from 'react'

export const CartContext = React.createContext();
export class CartProvider extends Component {
    constructor(props) {
        super(props);
        this.state = {
            numb : 0
        }
        this.addToCart = this.addToCart.bind(this);
    }
    addToCart(){
        this.setState({
            numb: this.state.numb + 1
        })
    }
    render() {
        return (
            <CartContext.Provider value={{
                number: this.state.numb,
                addToCart: this.addToCart
            }}>
                {this.props.children}
            </CartContext.Provider>
        )
    }
}
