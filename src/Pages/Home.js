import React from 'react'
import { MainMenu } from '../Components/MainMenu'
import { ListProduct } from '../Components/ListProduct'

export const Home = () => {
    return (
        <div>
            <MainMenu />
            <ListProduct />
        </div>
    )
}
